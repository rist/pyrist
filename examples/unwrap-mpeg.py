# pyRIST. Copyright 2019-2020 Mad Resistor LLP. All right reserved.
# Author: Kuldeep Singh Dhaka <kuldeep@madresistor.com>

"""Wrap MPEG stream

Usage:
  wrap-mpeg.py [--rist=<RIST_URL>] [--mpeg=<MPEG_URL>]
  wrap-mpeg.py (-h | --help)
  wrap-mpeg.py --version

Options:
  -r --rist=<RIST_URL>       RIST Sender URL [default: localhost:8001]
  -s --mpeg=<MPEG_URL>       MPEG Stream URL [default: localhost:8002]
"""

import rist
from docopt import docopt
import socket

args = docopt(__doc__)

def addr_port(s):
	a, p = s.split(':')
	return a, int(p)

MPEG_URL = args['--mpeg']
RIST_URL = args['--rist']

r = rist.Receiver()
r.peer_create({
	'address': RIST_URL
})

r.start()

u = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

MPEG_ADDR, MPEG_PORT = addr_port(MPEG_URL)

try:
	while True:
		data = r.data_read()
		if data is not None and len(data.payload):
			u.sendto(data.payload, (MPEG_ADDR, MPEG_PORT))
except:
	pass
