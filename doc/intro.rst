Introduction to pyRIST
======================

pyrist is a Python binding of `librist`_.

`librist`_ is a library that can be used to easily add the RIST protocol to your application.

.. image:: ../librist_logo.png
   :alt: librist logo

Importing
---------

.. code:: python

	import rist

.. _librist: https://code.videolan.org/rist/librist
